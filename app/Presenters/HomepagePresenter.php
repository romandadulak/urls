<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    public function __construct(
		private Nette\Database\Explorer $database,
	) {
	}

    public function renderDefault(): void
    {
        $this->template->urls = $this->database
            ->table('urls')
            ->order('created_at DESC')
            ->limit(50);
    }

    protected function createComponentAddForm(): Form
    {
        $form = new Form;
        $form->addText('url', 'Url:')
            ->setRequired();

        $form->addSubmit('send', 'Save');
        $form->onSuccess[] = [$this, 'urlsFormSucceeded'];

        return $form;
    }

    public function urlsFormSucceeded(Form $form): void
    {
        $data = $form->getValues();
        $date = new \DateTime();
        $data['created_at'] = $date;
        $data['used'] = 0;
        $generatedShort = $date->getTimestamp();
        $data['short'] = $generatedShort;
        \Tracy\Debugger::barDump($data);
        $this->database
            ->table('urls')
            ->insert($data);
    
        $this->flashMessage('Url was saved', 'success');
        $this->redirect('this');
    }

    public function actionR(string $id): void {
        $url = $this->database
		->table('urls')
		->get(["short" => $id]);

        $used = $url->used + 1;
        $url->update(['used' => $used]);
        $this->redirectUrl($url->url);
    }
}
