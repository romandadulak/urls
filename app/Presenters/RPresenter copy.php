<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;

final class RPresenter extends Nette\Application\UI\Presenter
{
    public function __construct(
		private Nette\Database\Explorer $database,
	) {
	}

    public function renderDefault(string $short): void
    {
        $url = $this->database
		->table('urls')
		->get(["short" => $short]);

        $used = $url->used + 1;
        $url->update(['used' => $used]);
        $this->redirectUrl($url->url);
    }
}
